import numpy as np
import logging
import time
from copy import deepcopy
from .linesearch import line_search_wolfe, line_search_weak_wolfe, line_search_goldstein_price

# Class running nonlinear conjugate gradient
# opt is an optimizable class that must provide the following functions:
#   getVariable(): current value of the optimzed variable
#   objectiveFun(): value of the objective function
#   updateTry(direction, step, [acceptThreshold]) computes a temporary variable by moving the current one in the direction 'dircetion' with step 'step'
#                                                 the temporary variable is not stored if the objective function is larger than acceptThreshold (when specified)
#                                                 This function should not update the current variable
#   acceptVarTry() replace the current variable by the temporary one
#   getGradient(coeff) returns coeff * gradient; the result can be used as 'direction' in updateTry
#
# optional functions:
#   startOptim(): called before starting the optimization
#   startOfIteration(): called before each iteration
#   endOfIteration() called after each iteration
#   endOptim(): called once optimization is completed
#   dotProduct(g1, g2): returns a list of dot products between g1 and g2, where g1 is a direction and g2 a list of directions
#                       default: use standard dot product assuming that directions are arrays
#   addProd(g0, step, g1): returns g0 + step * g1 for directions g0, g1
#   copyDir(g0): returns a copy of g0
#   randomDir(): Returns a random direction
# optional attributes:
#   gradEps: stopping theshold for small gradient
#   gradCoeff: normalizaing coefficient for gradient.
#
# verb: for verbose printing
# TestGradient evaluate accracy of first order approximation (debugging)
# epsInit: initial gradient step

def __dotProduct(x,y):
    res = []
    for yy in y:
        res.append((x*yy).sum())
    return res

def __addProd(x,y,a):
    return x + a*y

def __prod(x, a):
    return a*x

def __copyDir(x):
    return np.copy(x)

def __stopCondition():
    return True



def cg(opt, verb = True, maxIter=1000, TestGradient = False, epsInit=0.01, sgdPar=None, Wolfe=True,
       lineSearch = 'Weak_Wolfe'):

    if (hasattr(opt, 'getVariable')==False or hasattr(opt, 'objectiveFun')==False or hasattr(opt, 'updateTry')==False
            or hasattr(opt, 'acceptVarTry')==False or hasattr(opt, 'getGradient')==False):
        logging.error('Error: required functions are not provided')
        return

    # if Wolfe and hasattr(opt, 'dotProduct_euclidean'):
    #     dotProduct = opt.dotProduct_euclidean   #
    # elif not Wolfe and hasattr(opt, 'dotProduct'):
    #     dotProduct = opt.dotProduct
    # else:
    #     dotProduct = __dotProduct
    if lineSearch == "Wolfe":
        line_search = line_search_wolfe
    elif lineSearch == "Goldstein_Price":
        line_search = line_search_goldstein_price
    elif lineSearch == "Weak_Wolfe":
        line_search = line_search_weak_wolfe
    else:
        logging.warning('Unrecognized line search: using weak wolfe condition')
        line_search = line_search_weak_wolfe

    if hasattr(opt, 'dotProduct'):
        dotProduct = opt.dotProduct
    else:
        dotProduct = __dotProduct

    if not(hasattr(opt, 'stopCondition')):
        stopCondition = __stopCondition
    else:
        stopCondition = opt.stopCondition

    if not(hasattr(opt, 'prod')):
        prod = __prod
    else:
        prod = opt.prod


    if hasattr(opt, 'startOptim'):
        opt.startOptim()

    if hasattr(opt, 'gradEps'):
        gradEps = opt.gradEps
    elif hasattr(opt, 'options') and 'gradEps' in opt.options.keys():
        gradEps = opt.options['gradEps']
    else:
        gradEps = None

    if hasattr(opt, 'gradLB'):
        gradLB = opt.gradLB
    elif hasattr(opt, 'options') and 'gradLB' in opt.options.keys():
        gradLB = opt.options['gradLB']
    else:
        gradLB = 1e-4

    if hasattr(opt, 'gradLBCoeff'):
        gradLBCoeff = opt.gradLBCoeff
    elif hasattr(opt, 'options') and 'gradLBCoeff' in opt.options.keys():
        gradLBCoeff = opt.options['gradLBCoeff']
    else:
        gradLBCoeff = 1e-4


    if hasattr(opt, 'cgBurnIn'):
        cgBurnIn = opt.cgBurnIn
    else:
        cgBurnIn = 10
    
    if hasattr(opt, 'gradCoeff'):
        gradCoeff = opt.gradCoeff
    else:
        gradCoeff = 1.0

    if hasattr(opt, 'restartRate'):
        restartRate = opt.restartRate
    else:
        restartRate = 100

    if hasattr(opt, 'epsMax'):
        epsMax = opt.epsMax
    else:
        epsMax = 1.

    if sgdPar is None:
        sgd = False
    else:
        sgd = True
        sgdBurnIn = sgdPar[0]
        sgdRate = sgdPar[1]

    if sgd:
        restartRate = 1
        TestGradient = False

    eps = epsInit
    epsMin = 1e-10
    opt.converged = False

    if hasattr(opt, 'reset') and opt.reset:
        opt.obj = None

    obj = opt.objectiveFun()
    opt.reset = False
    #obj = opt.objectiveFun()
    logging.info('iteration 0: obj = {0: .5f}'.format(obj))
    # if (obj < 1e-10):
    #     return opt.getVariable()


    gval = None
    obj_old = None
    skipCG = 0
    noUpdate = 0
    it = 0
    itt0 = time.process_time()
    oldDir = None
    grdOld = None
    grdOld2 = None
    while it < maxIter:
        t0 = time.process_time()
        if it % restartRate == 0:
            skipCG = 1

        if hasattr(opt, 'startOfIteration'):
            opt.startOfIteration()

        if opt.reset:
            opt.obj = None
            obj = opt.objectiveFun()
            obj_old = None
            gval = None

        if noUpdate==0:
            if gval is None:
                grd = opt.getGradient(gradCoeff)
            else:
                grd = deepcopy(gval)

        if TestGradient:
            if hasattr(opt, 'randomDir'):
                dirfoo = opt.randomDir()
            else:
                dirfoo = np.random.normal(size=grd.shape)
            epsfoo = 1e-8
            objfoo1 = opt.updateTry(dirfoo, epsfoo, obj-1e10)
            objfoo2 = opt.updateTry(dirfoo, -epsfoo, obj-1e10)
            [grdfoo] = opt.dotProduct(grd, [dirfoo])
            logging.info('Test Gradient: %.6f %.6f' %((objfoo1 - objfoo2)/(2*epsfoo), -grdfoo * gradCoeff ))
        if sgd:
            eps = epsInit / (1 + sgdRate*max(0, it - sgdBurnIn))
            objTry = opt.updateTry(grd, eps, obj+1e10)
            opt.acceptVarTry()
            obj = objTry
            if verb | (it == maxIter):
                logging.info('iteration {0:d}: obj = {1:.5f}, eps = {2:.5f}'.format(it+1, obj, eps))

            if hasattr(opt, 'endOfIteration'):
                opt.endOfIteration()
            it += 1
        else:

            if it == 0 or it == cgBurnIn:
                [grdOld2] = opt.dotProduct(grd, [grd])
                grd2= grdOld2
                grdTry = np.sqrt(max(1e-20, grdOld2))
                oldDir = deepcopy(grd)
                grdOld = deepcopy(grd)
                dir0 = deepcopy(grd)
                # if hasattr(opt, 'copyDir'):
                #     oldDir = opt.copyDir(grd)
                #     grdOld = opt.copyDir(grd)
                #     dir0 = opt.copyDir(grd)
                # else:
                #     oldDir = np.copy(grd)
                #     grdOld = np.copy(grd)
                #     dir0 = np.copy(grd)
                beta = 0
            else:
                [grd2, grd12] = dotProduct(grd, [grd, grdOld])

                if skipCG == 0:
                    beta = max(0, (grd2 - grd12)/grdOld2)
                else:
                    beta = 0

                grdOld2 = grd2
                grdTry = np.sqrt(np.maximum(1e-20,grd2 + beta * grd12))

                if hasattr(opt, 'addProd'):
                    dir0 = opt.addProd(grd, oldDir, beta)
                else:
                    dir0 = grd + beta * oldDir
                oldDir = deepcopy(dir0)
                grdOld = deepcopy(grd)
                # if hasattr(opt, 'copyDir'):
                #     oldDir = opt.copyDir(dir0)
                #     grdOld = opt.copyDir(grd)
                # else:
                #     oldDir = np.copy(dir0)
                #     grdOld = np.copy(grd)

            if it == 0 or it == cgBurnIn:
                if gradEps is None:
                    gradEps = max(gradLBCoeff * np.sqrt(grd2), gradLB)
                else:
                    gradEps = min(gradEps, gradLBCoeff * np.sqrt(grd2))
                logging.info(f'Gradient threshold: {gradEps:.6f}')


            if it < cgBurnIn:
                Wolfe = False
                eps = epsInit
            else:
                if Wolfe:
                    eps = 1.
                else:
                    epsBig = epsMax / (grdTry)
                    if eps > epsBig:
                        eps = epsBig
            noUpdate = 0

            _eps = eps
            # for d in dir0['cAll']:
            #     print('dir0 ', d.keys())
            eps, fc, gc, phi_star, old_fval, gval = line_search(opt, dir0, gfk=grd, old_fval=obj,
                                                                old_old_fval=obj_old, c1=1e-4, c2=0.9, amax=None,
                                                                t_init=epsInit,
                                                                maxiter=10)
            if eps is not None:
                diffVar = prod(dir0, -eps)
                obj_old = obj
                opt.acceptVarTry()  #
                obj = phi_star
                newreset = False
            else:
                logging.info('Wolfe search unsuccessful')
                if opt.reset:
                    logging.info('Cannot go any further')
                    opt.converged = False
                    if hasattr(opt, 'endOfProcedure'):
                        opt.endOfProcedure()
                    elif hasattr(opt, 'endOfIteration'):
                        opt.endOfIteration()
                    break
                newreset = True
                gval = None

            if (np.fabs(obj - obj_old) < 1e-7) and stopCondition():
                logging.info(
                    f'iteration {it + 1:d}: obj = {obj:.5f}, eps = {eps:.5f}, gradient: {np.sqrt(grd2):.5f}')
                if it > cgBurnIn or opt.reset:
                    logging.info('Stopping Gradient Descent: small variation')
                    opt.converged = True
                    if hasattr(opt, 'endOfProcedure'):
                        opt.endOfProcedure()
                    elif hasattr(opt, 'endOfIteration'):
                        opt.endOfIteration()
                    break
                else:
                    newreset = True

            tt0 = time.process_time()
            t1 = tt0 - t0
            tt1 = tt0 - itt0
            itt0 = tt0
            if verb | (it == maxIter):
                if eps is None:
                    logging.info(
                        f'iteration {it + 1:d}: obj = {obj:.5f}, eps = None, gradient: {np.sqrt(grd2):.5f}, time = {t1:.04f}, {tt1:.04f}')
                else:
                    logging.info(
                        f'iteration {it + 1:d}: obj = {obj:.5f}, eps = {eps:.5f}, gradient: {np.sqrt(grd2):.5f}, time = {t1:.04f}, {tt1:.04f}')

            if np.sqrt(grd2) < gradEps and stopCondition():
                logging.info('Stopping Gradient Descent: small gradient')
                opt.converged = True
                if hasattr(opt, 'endOfProcedure'):
                    opt.endOfProcedure()
                elif hasattr(opt, 'endOfIteration'):
                    opt.endOfIteration()
                break
            if eps is not None:
                eps = np.minimum(100 * eps, epsMax)
            else:
                eps = epsMax

            opt.reset = newreset
            if hasattr(opt, 'endOfIteration'):
                opt.endOfIteration()
            if not opt.reset:
                it += 1
            # if opt.reset:
            #     opt.reset = False

    if it == maxIter and hasattr(opt, 'endOfProcedure'):
        opt.endOfProcedure()

    if hasattr(opt, 'endOptim'):
        opt.endOptim()

    return opt.getVariable()

def linearcg(op, b, iterMax=100, x0=None, param = None, verb=False, tol=1e-10):
    if x0 is None:
        x = np.zeros(b.shape)
    else:
        x = x0
    if param is None:
        z = op(x) - 2*b
    else:
        z = op(x, param) - 2*b

    ener = (z*x).sum()
    oldEner = ener
    f=0
    mu = 0
    r = -z - b
    for i2 in range(iterMax):
        if i2 == 0:
            mu = (r*r).sum()
            if mu < 1e-10:
                return x
            p = r
        else:
            muold = mu
            mu = (r*r).sum()
            beta = mu/muold
            p = r + beta * p

        if param is None:
            q = op(p)
        else:
            q = op(p, param)

        u = (p*q).sum()
        alpha = mu / u
        x += alpha * p
        r -= alpha * q
        z += alpha * q
        ener = (z*x).sum()
        if param is None:
            error = ((op(x)-b)**2).sum()
        else:
            error = ((op(x,param)-b)**2).sum()
        if verb and (i2%10) == 0:
            logging.info('iter {0:d} ener = {1:.6f} error = {2:.15f}'.format(i2+1, ener, error))
        if alpha * np.mean((np.fabs(p))) < tol:
            f = 1
            return x

        if np.fabs(ener - oldEner) < tol*np.fabs(oldEner):
            if verb:
                logging.info('iter {0:d} ener = {1:.6f} error = {2:.15f}'.format(i2+1, ener, error))
            f = 1
            return x
        oldEner = ener

    if verb:
        logging.info('iter {0:d} ener = {1:.6f} error = {2:.15f}'.format(i2+1, ener, error))
    return x

